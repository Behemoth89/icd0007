<?php


class Enviorments{
    const LANG_KEY = 'lang';
    const USER_KEY = 'user';

    private $messageStore;

    public function __construct(){
        if (isset($_COOKIE['lang'])){
            $this->messageStore = new MessageStore(self::LANG_KEY, $_COOKIE['lang']);
        } else{
            $this->messageStore = new MessageStore(self::LANG_KEY, 'et');
        }
    }

    public function changeLanguage($lang){
        $this->messageStore->switchLanguage($lang);
        setcookie('lang', $lang);
    }

    public function isAuthenticated(){
        if (isset($_COOKIE[session_name()]) && session_status() === PHP_SESSION_NONE){
            session_start();
        }
        return isset($_SESSION[self::USER_KEY]);
    }

    public function markAuthenticated($user){
        if (session_status() !== PHP_SESSION_ACTIVE){
            session_start();
        }
        $_SESSION[self::USER_KEY] = $user;
    }

    public function logOut(){
        if (!isset($_COOKIE[session_name()])){
            session_start();
        }
        if (session_status() === PHP_SESSION_NONE){
            session_start();
        }
        session_destroy();
    }

    public function getMessages(){
        return $this->messageStore->getAllMessages();
    }
}