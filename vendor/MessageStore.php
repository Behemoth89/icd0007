<?php

class MessageStore {

    private static $dictionaries;
    private $lang;
    private $dir;

    public function __construct($dir, $lang) {
        $this->lang = $lang;
        $this->dir = $dir;

        $this->loadFile($dir, $lang);
    }

    public function switchLanguage($lang) {
        $this->lang = $lang;
        $this->loadFile($this->dir, $lang);
    }

    private function loadFile($dir, $lang) {
        if (isset(self::$dictionaries[$lang])) {
            return;
        } else {
            self::$dictionaries[$lang] = [];
        }

        $lines = file(sprintf("%s/messages-%s.txt", $dir, $lang));

        foreach ($lines as $line) {
            list($key, $value) = explode('=', trim($line), 2);

            self::$dictionaries[$lang][$key] = $value;
        }
    }

    public function getAllMessages() {
        if (!isset(self::$dictionaries[$this->lang])) {
            return [];
        }

        $result = [];
        foreach (self::$dictionaries[$this->lang] as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    public function getMessage($key, $substitutions = []) {
        $dict = self::$dictionaries;

        if (!isset($dict[$this->lang])
            || !isset($dict[$this->lang][$key])) {
            return '';
        }

        $message = $dict[$this->lang][$key];

        $message = preg_replace_callback(
            '/\{(\d+)\}/',
            function ($matches) use ($substitutions) {
                $index = $matches[1] - 1; // counting starts from {1}.

                return isset($substitutions[$index])
                    ? $substitutions[$index] : '';
            },
            $message
        );

        return $message;
    }
}
