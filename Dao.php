<?php

class Dao
{
    private const USERNAME = 'behemoth89';
    private const PASSWORD = 'e480';
    private $address = 'mysql:host=db.mkalmo.xyz;dbname=behemoth89';
    private $connection;

    public function __construct(){
        $this->connection = new PDO($this->address, self::USERNAME, self::PASSWORD, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    function savePerson($person){
        $firstName = $person->getFirstName();
        $lastName = $person->getLastName();
        $stmt = $this->connection->prepare("INSERT INTO persons (firstName, lastName) VALUES ('$firstName', '$lastName')");
        $stmt->execute();
        $phones = $person->getPhonesAsList();
        if (!empty($phones)){
            $lastId = $this->connection->lastInsertId();
            foreach ($phones as $phone){
                $ph = $this->connection->prepare("INSERT INTO phones (phone, personID) VALUES ('$phone', '$lastId')");
                $ph->execute();
            }
        }
    }

    function getAllPersons(){
        $persons = $this->connection->prepare("SELECT * FROM persons");
        $persons->execute();
        $result = [];

        foreach ($persons as $p){
            $person = new Person($p['firstName'], $p['lastName']);
            $id = $p['id'];
            $person->setId($id);
            $result[$id] = $person;
        }
        $phones = $this->connection->prepare("SELECT * FROM phones");
        $phones->execute();

        foreach ($phones as $phone){
            $id = $phone['personID'];
            if (array_key_exists($id, $result)){
                $result[$id]->addPhone($phone['phone']);
            }
        }
        return $result;
    }

    function getPersonById($id){
        $idInt = intval($id);
        $query = $this->connection->prepare("SELECT * FROM persons WHERE id = $idInt");
        $query->execute();
        foreach ($query as $q){
            $person = new Person($q['firstName'], $q['lastName']);
            $person->setId($idInt);
        }

        $phones = $this->connection->prepare("SELECT * FROM phones WHERE personID = $id");
        $phones->execute();
        if (!empty($phones)){
            foreach ($phones as $phone) {
                $person->addPhone($phone['phone']);
            }
        }
        return $person;
    }

    function updatePersonById($id, $updatedPerson){
        $intId = intval($id);
        $oldPerson = $this->getPersonById($intId);
        if ($oldPerson->getFirstName() != $updatedPerson->getFirstName() or $oldPerson->getLastName() != $updatedPerson->getLastName()){
            $firstname = $updatedPerson->getFirstName();
            $lastname = $updatedPerson->getLastName();
            $update = $this->connection->prepare("UPDATE persons SET firstName = '$firstname', lastname = '$lastname' WHERE id = $intId");
            $update->execute();
        }
        $phones = $this->connection->prepare("DELETE FROM phones WHERE personID = $intId");
        $phones->execute();

        $newPhones = $updatedPerson->getPhonesAsList();

        if (!empty($newPhones)){
            foreach ($newPhones as $phone){
                $ph = $this->connection->prepare("INSERT INTO phones (phone, personID) VALUES ('$phone', '$intId')");
                $ph->execute();
            }
        }
    }


}