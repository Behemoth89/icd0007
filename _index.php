<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Harjutustund 1</title>
</head>
<body>
<br>
<div class="menu">
    <a href="_index.php" class="button" id="list-page-link">Nimekiri</a>
    <a href="_add.php" class="button" id="add-page-link">Lisa</a>
</div>
<br>
<hr>
<table>
    <thead>
        <tr>
            <th>Eesnimi</th>
            <th>Perekonnanimi</th>
            <th>Telefoninumber</th>
        </tr>
    </thead>
    <tbody>
<?php
    include_once('_dbConnection.php');
    $results = $connection->prepare("SELECT * FROM persons LEFT JOIN phones ON persons.id = phones.personID;");
    $results->execute();
    $phonesJoined = array();
    $namesJoined = array();

    foreach ($results as $result){
        if (!array_key_exists($result['phone'], $result)){
            if (array_key_exists($result['id'], $phonesJoined)){
                $phonesJoined[$result['id']][] = $result['phone'];
            } else {
                $phonesJoined[$result['id']] = array($result['phone']);
            }
        }
        if (!array_key_exists($result['id'], $namesJoined)){
            $namesJoined[$result['id']] = array("firstName" => $result['firstName'], "lastName" => $result['lastName']);
        }
    }
    foreach (array_keys($namesJoined) as $key){
        echo "<tr>";
        echo "<th>".urldecode($namesJoined[$key]['firstName'])."</th>";
        echo "<th>".urldecode($namesJoined[$key]['lastName'])."</th>";
        if (key_exists($key, $phonesJoined)){
            echo "<th>".urldecode(implode(", ", $phonesJoined[$key]))."</th>";
        }
        echo "</tr>";
    }
?>
    </tbody>
</table>
<br>
<div class="footer">
    This is a footer!
</div>
</body>
</html>

