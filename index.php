<?php
require_once 'vendor/tpl.php';
require_once 'functions.php';
require_once 'vendor/Request.php';
require_once 'Person.php';
require_once 'Dao.php';
require_once 'vendor/MessageStore.php';
require_once 'Enviorments.php';



//Default param
$firstName = "";
$lastName = "";
$phone1 = "";
$phone2 = "";
$phone3 = "";
$errors = [];
$persons = [];
$id = "";
$username = "";
$password = "";
$errorMessage = "";

$request = new Request($_REQUEST);
$dao = new Dao();
$command = $request->param('command');
$env = new Enviorments();

$changeLang = $request->param('changeLang');
if ($changeLang === 'et'){
    $env->changeLanguage('et');
} elseif ($changeLang === 'en'){
    $env->changeLanguage('en');
}

if ($command != 'login' && !$env->isAuthenticated()){
    $data['path'] = 'login.html';
    $data['loggedIn'] = false;
    print renderTemplate("tpl/main.html", $data, $env->getMessages());
    exit();
} elseif ($env->isAuthenticated()){
    $loggedIn = true;
}


$translations = $env->getMessages();


if ($command === 'show_list_page'){
    $path = "list.html";
    $persons = $dao->getAllPersons();
} elseif ($command === 'login'){
    $path = "login.html";
    if ($request->param('username') == 'user' && $request->param('password') == 'secret'){
        $env->markAuthenticated('user');
        header("Location: index.php?command=show_list_page");
        exit();
    } else {
        $path = "login.html";
        $username = $request->param('username');
        $password = $request->param('password');
        $errorMessage = $translations['LOGIN_ERROR'];
    }
} elseif ($command === 'show_add_page') {
    $path = "add.html";
} elseif ($command === 'edit'){
    $path = "add.html";
    $id = $request->param('id');
    $onePerson = $dao->getPersonById($id);
    $firstName = $onePerson->getFirstName();
    $lastName = $onePerson->getLastName();
    $phones1 = $onePerson->getPhonesAsList();
    list($phone1, $phone2, $phone3) = $phones1;
} elseif ($command === 'save') {
    if (validateNameLength(getValue($request, 'firstName'))){
        $errors[] = $translations['FIRST_NAME_ERROR'];
    }
    if (validateNameLength(getValue($request, 'lastName'))) {
        $errors[] = $translations['LAST_NAME_ERROR'];
    }
    if ($errors){
        $path = "add.html";
        $firstName = getValue($request, 'firstName');
        $lastName = getValue($request, 'lastName');
        $phone1 = getValue($request, 'phone1');
        $phone2 = getValue($request, 'phone2');
        $phone3 = getValue($request, 'phone3');
    } else {
        $path = "list.html";
        $person = new Person(getValue($request, 'firstName'), getValue($request, 'lastName'));
        $person->addPhone(getValue($request, 'phone1'));
        $person->addPhone(getValue($request, 'phone2'));
        $person->addPhone(getValue($request, 'phone3'));

        if ($request->param('id') === ""){
            $dao->savePerson($person);
            header('Location: index.php?command=show_list_page&add=success');
            exit();
        } else {
            $dao->updatePersonById($request->param('id'), $person);
        }
        $persons = $dao->getAllPersons();
    }
} elseif ($command === 'logout') {
    $env->logOut();
    header("Location: index.php");
    exit;
} else{
    $path = "list.html";
    $persons = $dao->getAllPersons();
}




$data = [
    'path' => $path,
    'firstName' => $firstName,
    'lastName' => $lastName,
    'phone1' => $phone1,
    'phone2' => $phone2,
    'phone3' => $phone3,
    'errors' => $errors,
    'persons' => $persons,
    'id' => $id,
    'username' => $username,
    'password' => $password,
    'errorMessage' => $errorMessage,
    'loggedIn' => $loggedIn
];


print renderTemplate("tpl/main.html", $data, $env->getMessages());
