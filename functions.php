<?php

function validateNameLength($name){
    if (strlen($name) < 2){
        return true;
    }
    return false;
}

function getValue($request, $key){
    return $request->param($key);
}

function isAuthenticated(){
    if (isset($_COOKIE[session_name()]) && session_status() === PHP_SESSION_NONE){
        session_start();
    }
    return isset($_SESSION['user']);
}

function markAuthenticated($username){
    if (session_status() !== PHP_SESSION_ACTIVE){
        session_start();
    }
    $_SESSION['user'] = $username;
}

function logOut(){
    if (session_status() === PHP_SESSION_NONE){
        session_start();
    }
    session_destroy();
}