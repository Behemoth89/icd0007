<?php
$firstNameError = false;
$lastNameError = false;
if (isset($_POST['firstName'])){
    if (strlen($_POST['firstName']) < 2){
        $firstNameError = true;
    }
}
if (isset($_POST['lastName'])){
    if (strlen($_POST['lastName']) < 2){
        $lastNameError = true;
    }
}
$mayRedirect = false;
if (!$firstNameError && !$lastNameError && $_SERVER['REQUEST_METHOD'] === 'POST'){
    $firstName = urlencode($_POST['firstName']);
    $lastName = urlencode($_POST['lastName']);
    $phone1 = urlencode($_POST['phone1']);
    $phone2 = urlencode($_POST['phone2']);
    $phone3 = urlencode($_POST['phone3']);


    include_once('_dbConnection.php');
    $stmt = $connection->prepare("INSERT INTO persons (firstName, lastName) VALUES ('$firstName','$lastName')");
    $stmt->execute();
    $lastId = $connection->lastInsertId();
    $phones = array($phone1, $phone2, $phone3);
    foreach ($phones as $phone){
        $ph = $connection->prepare("INSERT INTO phones (phone, personID) VALUES ('$phone', '$lastId')");
        $ph->execute();
    }
    $mayRedirect = true;
    }
if ($mayRedirect){header("Location: index.php");}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Lisa</title>
</head>
<body>
<br>
<div class="menu">
    <a href="index.php" class="button" id="list-page-link">Nimekiri</a>
    <a href="_add.php" class="button" id="add-page-link">Lisa</a>
</div>
<br>
<hr>
<div class="input_form">
    <form method="post" action="_add.php">
        Eesnimi:<br>
            <input type="text" name="firstName" value="<?php if (isset($_POST['firstName'])) {echo $_POST['firstName'];} ?>"><br>
            <span class="error" id="error-block"><?php if ($firstNameError){echo "Eesnimi peab olema vähemalt 2 tähemärki!<br>";} ?></span><br>
        Perekonnanimi:<br>
            <input type="text" name="lastName" value="<?php if (isset($_POST['lastName'])) {echo $_POST['lastName'];} ?>"><br>
            <span class="error" id="error-block"><?php if ($lastNameError){echo "Perekonnanimi peab olema vähemalt 2 tähemärki!<br>";} ?></span><br>
        Telefoni numbrid:<br>
            <input type="text" name="phone1" value="<?php if (isset($_POST['phone1'])){echo $_POST['phone1'];} ?>"><br>
            <input type="text" name="phone2" value="<?php if (isset($_POST['phone2'])){echo $_POST['phone2'];} ?>"><br>
            <input type="text" name="phone3" value="<?php if (isset($_POST['phone3'])){echo $_POST['phone3'];} ?>"><br>
        <input type="submit" name="submitButton" value="Salvesta">
    </form>
</div>
<br>
<div class="footer">
    This is a footer!
</div>
</body>
</html>


