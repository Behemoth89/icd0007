<?php

require_once '../vendor/tpl.php';

$translations = ['red' => 'Punane', 'blue' => 'Sinine'];



// mallide teegi kasutamise näide
// $data['fileName'] = 'content.html';
// $data['color'] = 'Kollane';

if (isset($_POST['color'])) {

    header('Location: ?color='.$_POST['color']);
} else if (isset($_GET['color'])) {

    $data['color'] = $translations[$_GET['color']];

    print renderTemplate('content.html', $data);
} else {
    print renderTemplate('form.html');
}
