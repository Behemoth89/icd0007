<?php

require_once 'Person.php';

function statementToPersonList($stmt) {

    $dictionary = [];
    $personList = [];

    foreach ($stmt as $row) {
        $id = $row['id'];
        $name = $row['name'];
        $number = $row['number'];

        if (isset($personList[$id])) {
            $personList[$id]->addPhone($number);
        } else {
            $per = new Person($id, $name);
            $per->addPhone($number);
            $personList[$id] = $per;
        }
    }

    return $personList;
}
