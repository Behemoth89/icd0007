<?php

class Person
{
    private $firstName;
    private $lastName;
    private $phones;
    private $id;
    private $phonesAsString;

    public function __construct($firstName, $lastName){
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    function addPhone($phone){
        if ($phone == ""){
            return;
        }
        if ($this->phones){
            $this->phones[] = $phone;
        } else {
            $this->phones = array($phone);
        }
        $this->phonesAsString = join(", ", $this->phones);
    }

    function getFirstName(){
        return $this->firstName;
    }

    function getLastName(){
        return $this->lastName;
    }

    function getPhonesAsList(){
        return $this->phones;
    }

    function getId(){
        return $this->id;
    }

    function getPhonesAsString(){
        return $this->phonesAsString;
    }

    function setId($id){
        $this->id = $id;
    }
}